package models

import (
	"net/http"
	"github.com/go-chi/chi"
	"fmt"
	"gopkg.in/mgo.v2/bson"
)

func DeletePerson (w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "personID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id:", id)
	newSession := session.Copy()
	defer newSession.Close()
	peopleColl := newSession.DB("testdb").C("people")

	if err := peopleColl.Remove(bson.M{"_id": bson.ObjectIdHex(id)}); err != nil {
		http.Error(w, http.StatusText(500), 500)
		fmt.Println("can't delete:", err)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v deleted successfully\n", id)
}

func DeleteBook (w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "bookID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id:", id)
	newSession := session.Copy()
	defer newSession.Close()
	booksColl := newSession.DB("testdb").C("books")

	if err := booksColl.Remove(bson.M{"_id": bson.ObjectIdHex(id)}); err != nil {
		http.Error(w, http.StatusText(500), 500)
		fmt.Println("can't delete:", err)
		return
	}
	fmt.Fprintf(w, "Book with ID = %v deleted successfully\n", id)
}


// ----------sql-----------------
// result, err := db.Exec("DELETE FROM people WHERE userID = ?", id)
// if err != nil {
// 	http.Error(w, http.StatusText(500), 500)
// 	return
// }
// rowsAffected, err := result.RowsAffected()
// if err != nil {
// 	http.Error(w, http.StatusText(500), 500)
// 	return
// }
// fmt.Fprintf(w, "Person with ID = %v deleted successfully (%d row affected)\n", id, rowsAffected)