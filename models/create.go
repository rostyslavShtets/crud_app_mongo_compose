package models

import (
	"net/http"
	"io/ioutil"
	"log"
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2/bson"
)

func CreatePerson(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	var person Person
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &person)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
		fmt.Println(person)
		//check for empty values
	if person.LastName == "" || person.FirstName == "" || person.Age == 0 {
		fmt.Println("unvalid json")
		http.Error(w, http.StatusText(400), 400)
		return
	}
	person.Id = bson.NewObjectId()

	// copy session
	newSession := session.Copy()
	defer newSession.Close()
	peopleColl := newSession.DB("testdb").C("people")
	if err := peopleColl.Insert(person); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", person.Id)
}

func CreateBook(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	var book Book
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &book)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
		fmt.Println(book)
			
	book.Id = bson.NewObjectId()
	
	// copy session
	newSession := session.Copy()
	defer newSession.Close()
	booksColl := newSession.DB("testdb").C("books")
	if err := booksColl.Insert(book); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", book.Id)
}


// ---------------for mySQL-------
	
	// body, err := ioutil.ReadAll(r.Body)
	// var person Person
	// if err != nil {
	// 	log.Println("handlers SaveIDID error:", err)
	// 	http.Error(w, "can’t read body", http.StatusBadRequest)
	// 	return
	// }
	// err = json.Unmarshal(body, &person)
	// if err != nil {
	// 	log.Println("handlers SaveID error:", err)
	// 	http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
	// 	return
	// }
	// fmt.Println(person)

	// //check for empty values
	// if person.LastName == "" || person.FirstName == "" || person.Age == 0 {
	// 	fmt.Println("unvalid json")
	// 	http.Error(w, http.StatusText(400), 400)
	// 	return
	// }
	// result, err := db.Exec("INSERT INTO people (FirstName, LastName, Age) VALUES(?, ?, ?)",
	// 	person.FirstName, person.LastName, person.Age)
	// if err != nil {
	// 	fmt.Println("error with EXEC")
	// 	http.Error(w, http.StatusText(500), 500)
	// 	return
	// }
	// rowsAffected, err := result.RowsAffected()

	// if err != nil {
	// 	http.Error(w, http.StatusText(500), 500)
	// 	return
	// }
	// fmt.Fprintf(w, "Person %v created successfully (%d row affected)\n", person.UserID, rowsAffected)