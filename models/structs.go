package models
import "gopkg.in/mgo.v2/bson"

type Person struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	UserID string		`json:"userID"`
	FirstName string	`json:"firstName"`
	LastName string		`json:"lastName"`
	Age int				`json:"age"`
}

type Book struct {
	Id bson.ObjectId	`json:"id" bson:"_id,omitempty"`
	ISBN    string   	`json:"isbn"`
    Title   string   	`json:"title"`
    Author string 	 	`json:"author"`
    Price   string   	`json:"price"`
}