#!/usr/bin/env bash


set -x
set -e
docker-compose down
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o main

# docker build -t myapp_compose -f Dockerfile.scratch .
docker build -t myapp_compose_mongodb .
docker-compose up

#add date to database
#INSERT INTO people(FirstName, LastName, Age) VALUES ('John', 'Docker', 35);