package main

import (
	"users_crud_compose_mongo/models"
	"github.com/go-chi/chi"
	"net/http"
	"fmt"
	"log"
)
const port = ":8081"

func main() {
	models.Hello()
	fmt.Println("new_new")
	models.InitMongoDB("mongodb:27017")
			
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(".root"))
	})

	//request for people
	r.Get("/people", models.GetPeople)
	r.Get("/people/{personID}", models.GetPerson)
	r.Post("/people", models.CreatePerson)
	r.Put("/people/{personID}", models.UpdatePerson)
	r.Delete("/people/{personID}", models.DeletePerson)
	
	//request for books
	r.Get("/books", models.GetBooks)
	r.Get("/books/{bookID}", models.GetBook)
	r.Post("/book", models.CreateBook)
	r.Delete("/books/{bookID}", models.DeleteBook)


	fmt.Printf("Serv on port %v....\n", port)
	log.Fatal(http.ListenAndServe(port, r))
}

